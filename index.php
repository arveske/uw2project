@extends('base')
@section('cssContent')
    <link rel="stylesheet" href="css/stylesheet.css">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">
    <link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
@endsection
@section('mainContent')
<!-- ##### Welcome Area Start ##### -->
<div class="welcome_area bg-img background-overlay">
    <div class="breadcumb_area breadcumb-style-two bg-img" style="background-image: url(img/bg-img/breadcumb2.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div style="padding-top:60px" class="col-12">
                    <div class="hero-content">
                        <h6 align="center"><img src="img\core-img\logo text NB.png" alt="Logo text"> </h6>

                        <h4 align="center"><b>The best subscription based marketplace to Buy, Sell, and Trade sportcards!</b></h4>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    <!-- ##### Welcome Area End ##### -->

    <?php
    $sports_arr = array();

    $sports_arr[] = "Baseball";
    $sports_arr[] = "Basketball";
    $sports_arr[] = "Football";


    $position = array();

    $position['Baseball'][]="";
    $position['Baseball'][]="Other";
    $position['Baseball'][]="Atlanta Braves";
    $position['Baseball'][]="Miami Marlins";
    $position['Baseball'][]="New York Mets";
    $position['Baseball'][]="Philadelphia Phillies";
    $position['Baseball'][]="Washington Nationals";
    $position['Baseball'][]="Chicago Cubs";
    $position['Baseball'][]="Cincinnati Reds";
    $position['Baseball'][]="Milwaukee Brewers";
    $position['Baseball'][]="Pittsburgh Pirates";
    $position['Baseball'][]="St. Louis Cardinals";
    $position['Baseball'][]="Arizona Diamondbacks";
    $position['Baseball'][]="Colorado Rockies";
    $position['Baseball'][]="Los Angeles Dodgers";
    $position['Baseball'][]="San Diego Padres";
    $position['Baseball'][]="San Francisco Giants";
    $position['Baseball'][]="Baltimore Orioles";
    $position['Baseball'][]="Boston Red Sox";
    $position['Baseball'][]="New York Yankees";
    $position['Baseball'][]="Tampa Bay Rays";
    $position['Baseball'][]="Toronto Blue Jays";
    $position['Baseball'][]="Chicago White Sox";
    $position['Baseball'][]="Cleveland Indians";
    $position['Baseball'][]="Detroit Tigers";
    $position['Baseball'][]="Kansas City Royals";
    $position['Baseball'][]="Minnesota Twins";
    $position['Baseball'][]="Houston Astros";
    $position['Baseball'][]="Los Angeles Angels";
    $position['Baseball'][]="Oakland Athletics";
    $position['Baseball'][]="Seattle Mariners";
    $position['Baseball'][]="Texas Rangers";

    $position['Basketball'][]="";
    $position['Basketball'][]="Other";
    $position['Basketball'][]="Atlanta Hawks";
    $position['Basketball'][]="Boston Celtics";
    $position['Basketball'][]="Brooklyn Nets";
    $position['Basketball'][]="Charlotte Hornets";
    $position['Basketball'][]="Chicago Bulls";
    $position['Basketball'][]="Cleveland Cavaliers";
    $position['Basketball'][]="Dallas Mavericks";
    $position['Basketball'][]="Denver Nuggets";
    $position['Basketball'][]="Detroit Pistons";
    $position['Basketball'][]="Golden State Warriors";
    $position['Basketball'][]="Houston Rockets";
    $position['Basketball'][]="Indiana Pacers";
    $position['Basketball'][]="LA Clippers";
    $position['Basketball'][]="LA Lakers";
    $position['Basketball'][]="Memphis Grizzlies";
    $position['Basketball'][]="Miami Heat";
    $position['Basketball'][]="Milwaukee Bucks";
    $position['Basketball'][]="Minnesota Timberwolves";
    $position['Basketball'][]="New Orleans Hornets";
    $position['Basketball'][]="New York Knicks";
    $position['Basketball'][]="Oklahoma City Thunder";
    $position['Basketball'][]="Orlando Magic";
    $position['Basketball'][]="Philadelphia Sixers";
    $position['Basketball'][]="Phoenix Suns";
    $position['Basketball'][]="Portland Trail Blazers";
    $position['Basketball'][]="Sacramento Kings";
    $position['Basketball'][]="San Antonio Spurs";
    $position['Basketball'][]="Toronto Raptors";
    $position['Basketball'][]="Utah Jazz";
    $position['Basketball'][]="Washington Wizards";

    $position['Football'][]="";
    $position['Football'][]="Other";
    $position['Football'][]="Arizona Cardinals";
    $position['Football'][]="Atlanta Falcons";
    $position['Football'][]="Baltimore Ravens";
    $position['Football'][]="Buffalo Bills";
    $position['Football'][]="Carolina Panthers";
    $position['Football'][]="Chicago Bears";
    $position['Football'][]="Cincinnati Bengals";
    $position['Football'][]="Cleveland Browns";
    $position['Football'][]="Dallas Cowboys";
    $position['Football'][]="Denver Broncos";
    $position['Football'][]="Detroit Lions";
    $position['Football'][]="Green Bay Packers";
    $position['Football'][]="Houston Texans";
    $position['Football'][]="Indianapolis Colts";
    $position['Football'][]="Jacksonville Jaguars";
    $position['Football'][]="Kansas City Chiefs";
    $position['Football'][]="Los Angeles Rams";
    $position['Football'][]="Miami Dolphins";
    $position['Football'][]="Minnesota Vikings";
    $position['Football'][]="New England Patriots";
    $position['Football'][]="New Orleans Saints";
    $position['Football'][]="New York Giants";
    $position['Football'][]="New York Jets";
    $position['Football'][]="Oakland Raiders";
    $position['Football'][]="Philadelphia Eagles";
    $position['Football'][]="Pittsburgh Steelers";
    $position['Football'][]="San Diego Chargers";
    $position['Football'][]="San Francisco 49ers";
    $position['Football'][]="Seattle Seahawks";
    $position['Football'][]="Tampa Bay Buccaneers";
    $position['Football'][]="Tennessee Titans";
    $position['Football'][]="Washington Redskins";

    $service_arr = array();
    $service_arr[] ="USPS First Class Package (2-3 business days)";
    $service_arr[] ="USPS Priority Mail (1-3 business days)";
    $service_arr[] ="USPS Priority Mail Flat rate envelope (1-3 business days)";
    $service_arr[] ="USPS Priority Mail Small Flat rate box (1-3 business days)";
    $service_arr[] ="USPS Priority Mail Medium Flat rate box (1-3 business days)";
    $service_arr[] ="USPS Priority Mail Large Flat rate box (1-3 business days)";
    $service_arr[] ="USPS Priority Mail Padded Flat rate envelope (1-3 business days)";
    $service_arr[] ="UPS Ground (1-5 business days) (1-3 business days)";
    $service_arr[] ="FedEx Ground or Home Delivery (1-5 business days)";

    ?>

    <!-- #### Sell Form Start #### -->
    <div class="responsive_form" >
        <form action="{{ url('auction/add') }}" method="POST" enctype="multipart/form-data" id="auction-add" class="form-inline">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <div class="container">
                @include('sign.flash-message')
                <hr class="my-5">
                <h3>Item Description</h3>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Title</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <input class="form-control" type="text" name="title" maxlength="88" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Category</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <select id="category" class="form-control" size="0" name="category" required>
                                <option selected disabled value=""></option>
                                <?php foreach($sports_arr as $sa) { ?>
                                <option value="<?php echo $sa; ?>"><b><?php echo $sa; ?></b></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Item Type</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <select id="item-type" class="form-control" size="0" name="item-type" required>
                                <option selected disabled value=""></option>
                                <option value="Sport Card">Sport Card</option>
                                <option value="Memorabilia">Memorabilia</option>
                                <option value="Box Break">Box Break</option>
                            </select>
                        </div>
                    </div>
                    <hr class="my-5">
                    <h3>Item Details</h3>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Team</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <select id="team" class="form-control" name="team">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Player</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <input class="form-control" type="text" name="player" maxlength="60">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Manufacturer</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <select id="manufacturer" class="form-control" size="0" name="manufacturer">
                                <option value=""></option>
                                <option value="Topps">Topps</option>
                                <option value="Bowman">Bowman</option>
                                <option value="Panini">Panini</option>
                                <option value="Leaf">Leaf</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Graded</b></label>
                        <div class="col-md-10 col-sm-10 radio_btn">
                            <div id="graded">
                                <label class="radio_inline graded">
                                    <input type="radio" name="graded" value=1>Yes</label>
                                <label class="radio_inline graded">
                                    <input type="radio" name="graded" value=0>No</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Reprint</b></label>
                        <div class="col-md-10 col-sm-10 radio_btn">
                            <div id="reprint">
                                <label class="radio_inline">
                                    <input type="radio" name="reprint" value=1>Yes</label>
                                <label class="radio_inline">
                                    <input type="radio" name="reprint" value=0>No</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Description</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <textarea class="form-control" id='tinyMceExample' style="max-width:100%;" name="description" rows="6" required></textarea>
                            </textarea>
                        </div>
                    </div>
                    <hr class="my-5">
                    <h3>Photos</h3>
                    <div class="form-group row">
                        <div class="col-md-10 col-sm-10 wrapper ">

                           <!--
                              {{--  <form method="post" action="#" enctype="multipart/form-data" autocomplete="off">  --}}
                        <input type='file' name="file" id="file" onchange="filePreview(this)"/>
                        <input type="hidden" name="rotation" id="rotation" value="0"/>
                                <!-- <img id="blah" src="#" alt="your image" />
                                {{--  </form>  --}}
                        <div class="img-preview" style="display: block;">
                        <button id="rleft" type="button">Left</button>
                        <button id="rright" type="button">Right</button>
                        <div id="imgPreview"></div>

                        {{--  <form method="post" action="#" enctype="multipart/form-data" autocomplete="off">  --}}
                        <input type='file' name="file" id="file1" onchange="filePreview1(this)"/>
                        <input type="hidden" name="rotation" id="rotation1" value="0"/>

                                {{--  </form>  --}}
                        <div class="img-preview1" style="display: block;">
                        <button id="rleft1" type="button">Left</button>
                        <button id="rright1" type="button">Right</button>
                        <div id="imgPreview1"></div>
                        */


                    </div>
                    -->


                    <div class="container-file">
                        <div id="file1" style="position: relative;">
                          <input class="custom-file-input2" type='file' name="file" accept=".png, .jpg, .jpeg" onchange="filePreview(this, 'main-file', 1)" required/>
                          <div class="img-preview main-file" style="display: block;">
                          <div id="main-file"></div>
                          </div>
                        </div>
                        <div id="file1-rotate">
                          <button class="rright" id="rotate1-left" type="button" disabled>left</button>
                          <span>rotate</span>
                          <button class="rright" id="rotate1-right" type="button" disabled>right</button>
                        </div>
                        <div id="file2" style="position: relative;">
                          <input class="custom-file-input1 custom-in1" type='file' name="file" accept=".png, .jpg, .jpeg" onchange="filePreview(this, 'adder1', 2)"/>
                          <div class="img-preview1 adder1 prev-rot" style="display: block;">
                          <div id="adder1"></div>
                          <button class="rright1" id="rotate2" type="button" style="display: none;">rotate</button>
                          </div>
                        </div>
                        <div id="file3" style="position: relative;">
                          <input class="custom-file-input1 custom-in2" type='file' name="file" accept=".png, .jpg, .jpeg" onchange="filePreview(this, 'adder2', 3)"/>
                          <div class="img-preview2 adder2 prev-rot" style="display: block;">
                          <div id="adder2"></div>
                          <button class="rright1" id="rotate3" type="button" style="display: none;">rotate</button>
                          </div>
                        </div>
                        <div id="file4" style="position: relative;">
                          <input class="custom-file-input1 custom-in3" type='file' name="file" accept=".png, .jpg, .jpeg" onchange="filePreview(this, 'adder3', 4)"/>
                          <div class="img-preview3 adder3 prev-rot" style="display: block;">
                          <div id="adder3"></div>
                          <button class="rright1" id="rotate4" type="button" style="display: none;">rotate</button>
                          </div>
                        </div>
                        <div id="file5" style="position: relative;">
                          <input class="custom-file-input1 custom-in4" type='file' name="file" accept=".png, .jpg, .jpeg" onchange="filePreview(this, 'adder4', 5)"/>
                          <div class="img-preview4 adder4 prev-rot" style="display: block;">
                          <div id="adder4"></div>
                          <button class="rright1" id="rotate5" type="button" style="display: none;">rotate</button>
                          </div>
                        </div>
                    </div>
                        </div>
                    </div>

                    <hr class="my-5">
                    <h3>Sell Details</h3>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Format</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <select id="format" class="form-control" size="0" name="format" required>
                                <option selected disabled value=""></option>
                                <option value="Auction"><b>Auction</b></option>
                                <option value="Fixed Price"><b>Fixed Price</b></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Start Price / Price</b></label>
                        <div class="col-md-10 col-sm-10 two_sections">
                            <span>$</span>
                            <input class="form-control" type="number" name="start-price" min="0" max="99999" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Allow Offers</b></label>
                        <div class="col-md-10 col-sm-10 radio_btn">
                            <div id="allow-offers">
                                <label class="radio_inline">
                                    <input type="radio" name="allow-offers" value=1>Yes</label>
                                <label class="radio_inline">
                                    <input type="radio" name="allow-offers" checked value=0>No</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Duration</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <select id="duration" class="form-control" size="0" name="item-type" required>
                                <option selected disabled value=""></option>
                                <option value="5 days">5 days</option>
                                <option value="7 days">7 days</option>
                                <option value="10 days">10 days</option>
                            </select>
                        </div>
                    </div>
                    <hr class="my-5">
                    <h3>Shipping Details</h3>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Service</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <select id="service" class="form-control" size="0" name="service" required>
                                <option selected disabled value=""></option>
                                <?php foreach($service_arr as $sa) { ?>
                                <option value="<?php echo $sa; ?>"><b><?php echo $sa; ?></b></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Location Zip</b></label>
                        <div class="col-md-10 col-sm-10 ">
                            <input class="form-control" id="location-zip" name="location-zip" required
                            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            type = "number"
                            maxlength = "10"
                            min="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Package weight</b></label>
                        <div class="col-md-10 col-sm-10 four_sections">
                            <input class="form-control" id="package-weight-lbs" name="package-weight-lbs" required
                            type = "number"
                            min="0">
                            <span>lbs</span>
                            <input class="form-control" id="package-weight-oz" name="package-weight-oz" required
                            type = "number"
                            min="0">
                            <span>oz</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Package dimensions</b></label>
                        <div class="col-md-10 col-sm-10 five_sections">

                            <input class="form-control col-md-2 col-sm-2" id="location-dimensions-1" name="location-dimensions-1" required
                            type = "number"
                            min="0">
                            <span>x</span>
                            <input class="form-control col-md-2 col-sm-2" id="location-dimensions-2" name="location-dimensions-2" required
                            type = "number"
                            min="0">
                            <span>x</span>
                            <input class="form-control col-md-2 col-sm-2" id="location-dimensions-3" name="location-dimensions-3" required
                            type = "number"
                            min="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Shipping Cost</b></label>
                        <div class="col-md-10 col-sm-10 two_sections">
                            <span>$</span>
                            <input class="form-control" id="ship-cost" type="number" name="ship-cost" min="0" max="99999" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-2 col-form-label form-control-label"><b>Free Shipping</b></label>
                        <div class="col-md-10 col-sm-10 radio_btn">
                            <div id="free-shipping">
                                <label class="radio_inline">
                                    <input type="radio" name="free-shipping-1" id="free-shipping-1" value=1>Yes</label>
                                <label class="radio_inline">
                                    <input type="radio" name="free-shipping-0" id="free-shipping-0" checked value=0>No</label>
                            </div>
                        </div>
                    </div>
                    <p>Seller will provide tracking when the item is paid for.</p>
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Submit</button>
                        <a href="{{ url('items?type=Baseball') }}"><button class="btn btn-default btn-cancel" type="button">Cancel</button></a>
                    </div>
            </div>
        </form>
    </div>
    <!-- Shipping Details END -->
    <br>
    <br>
    <!--#### Sell Form End #### -->
@endsection
@section('javascriptContent')
<script type="text/javascript">
var category= document.getElementById("category");
var team = document.getElementById("team");
onchange(); //Change options after page load
category.onchange = onchange; // change options when s1 is changed

function onchange() {
    <?php foreach ($sports_arr as $sa) {?>
        if (category.value == '<?php echo $sa; ?>') {
            option_html = "";
            <?php if (isset($position[$sa])) { ?> // Make sure position is exist
                <?php foreach ($position[$sa] as $value) { ?>
                    option_html += "<option><?php echo $value; ?></option>";
                <?php } ?>
            <?php } ?>
            team.innerHTML = option_html;
        }
    <?php } ?>
}

$(document).on("click", "input[name='graded']", function(){
    thisRadio = $(this);
    if (thisRadio.hasClass("imChecked")) {
        thisRadio.removeClass("imChecked");
        thisRadio.prop('checked', false);
    } else {
        thisRadio.prop('checked', true);
        thisRadio.addClass("imChecked");
    };
})

$(document).on("click", "input[name='reprint']", function(){
    thisRadio = $(this);
    if (thisRadio.hasClass("imChecked")) {
        thisRadio.removeClass("imChecked");
        thisRadio.prop('checked', false);
    } else {
        thisRadio.prop('checked', true);
        thisRadio.addClass("imChecked");
    };
})

$('#free-shipping-0').click(function()
{
  $('#free-shipping-0').prop('checked', true);
  $('#free-shipping-1').prop('checked', false);
  $('#ship-cost').removeAttr("disabled");
  $('#ship-cost').removeAttr("value");
  $('#location-dimensions-1').removeAttr("disabled");
  $('#location-dimensions-2').removeAttr("disabled");
  $('#location-dimensions-3').removeAttr("disabled");
  $('#package-weight-lbs').removeAttr("disabled");
  $('#package-weight-oz').removeAttr("disabled");
  $('#location-zip').removeAttr("disabled");
  $('#service').removeAttr("disabled");
  $('#ship-cost').removeAttr("disabled");
  $('#ship-cost').removeAttr("value");
  $('#location-dimensions-1').attr("required","required");
  $('#location-dimensions-2').attr("required","required");
  $('#location-dimensions-3').attr("required","required");
  $('#package-weight-lbs').attr("required","required");
  $('#package-weight-oz').attr("required","required");
  $('#location-zip').attr("required","required");
});

$('#free-shipping-1').click(function()
{
  $('#free-shipping-0').prop('checked', false);
  $('#free-shipping-1').prop('checked', true);
  $('#ship-cost').attr("disabled","disabled");
  $('#ship-cost').attr("value","0");
  $('#ship-cost').val('0');
  $('#location-dimensions-1').val('');
  $('#location-dimensions-1').attr("disabled","disabled");
  $('#location-dimensions-2').val('');
  $('#location-dimensions-2').attr("disabled","disabled");
  $('#location-dimensions-3').val('');
  $('#location-dimensions-3').attr("disabled","disabled");
  $('#package-weight-lbs').val('');
  $('#package-weight-lbs').attr("disabled","disabled");
  $('#package-weight-oz').val('');
  $('#package-weight-oz').attr("disabled","disabled");
  $('#location-zip').val('');
  $('#location-zip').attr("disabled","disabled");
  $('#service').val('');
  $('#service').attr("disabled","disabled");
  $('#location-dimensions-1').removeAttr("required");
  $('#location-dimensions-2').removeAttr("required");
  $('#location-dimensions-3').removeAttr("required");
  $('#package-weight-lbs').removeAttr("required");
  $('#package-weight-oz').removeAttr("required");
  $('#location-zip').removeAttr("required");
});


    $(function() {
        var rotation = 0;
        $("#rotate1-left").click(function() {
            rotation = (rotation - 90) % 360;
            $(".pic-view1").css({ 'transform': 'rotate(' + rotation + 'deg)' });

            if (rotation != 0) {
                $(".pic-view1").css({ 'width': '400px', 'height': '270px' });
            } else {
                $(".pic-view1").css({ 'width': '400px', 'height': '270px' });
            }
            $('#rotation').val(rotation);
        });

        $("#rotate1-right").click(function() {
            rotation = (rotation + 90) % 360;
            $(".pic-view1").css({ 'transform': 'rotate(' + rotation + 'deg)' });

            if (rotation != 0) {
                $(".pic-view1").css({ 'width': '400px', 'height': '270x' });
            } else {
                $(".pic-view1").css({ 'width': '400px', 'height': '270px' });
            }
            $('#rotation').val(rotation);
        });
    });

    $(function() {
        var rotation = 0;
        $("#rotate2").click(function() {
            rotation = (rotation + 90) % 360;
            $(".pic-view2").css({ 'transform': 'rotate(' + rotation + 'deg)' });

            if (rotation != 0) {
                $(".pic-view2").css({ 'width': '193px', 'height': '140px' });
            } else {
                $(".pic-view2").css({ 'width': '193px', 'height': '140px' });
            }
            $('#rotation1').val(rotation);
        });
    });

    $(function() {
        var rotation = 0;
        $("#rotate3").click(function() {
            rotation = (rotation + 90) % 360;
            $(".pic-view3").css({ 'transform': 'rotate(' + rotation + 'deg)' });

            if (rotation != 0) {
                $(".pic-view3").css({ 'width': '193px', 'height': '140px' });
            } else {
                $(".pic-view3").css({ 'width': '193px', 'height': '140px' });
            }
            $('#rotation2').val(rotation);
        });
    });

    $(function() {
        var rotation = 0;
        $("#rotate4").click(function() {
            rotation = (rotation + 90) % 360;
            $(".pic-view4").css({ 'transform': 'rotate(' + rotation + 'deg)' });

            if (rotation != 0) {
                $(".pic-view4").css({ 'width': '193px', 'height': '140px' });
            } else {
                $(".pic-view4").css({ 'width': '193px', 'height': '140px' });
            }
            $('#rotation3').val(rotation);
        });
    });

    $(function() {
        var rotation = 0;
        $("#rotate5").click(function() {
            rotation = (rotation + 90) % 360;
            $(".pic-view5").css({ 'transform': 'rotate(' + rotation + 'deg)' });

            if (rotation != 0) {
                $(".pic-view5").css({ 'width': '193px', 'height': '140px' });
            } else {
                $(".pic-view5").css({ 'width': '193px', 'height': '140px' });
            }
            $('#rotation4').val(rotation);
        });
    });


    $("#file").change(function() {
        // Validate file type
        var filePath = this.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if (!allowedExtensions.exec(filePath)) {
            // alert('Sorry, only JPG/JPEG/PNG/GIF files are allowed to upload.');
            toastr.warning('Sorry, only JPG/JPEG/PNG/GIF files are allowed to upload.');
            this.value = '';
        }

        // Image preview
        filePreview(this);
    });

    var filesArray = [];
    var mainIndex = 1;

    function filePreview(input, place, fileIndex) {
        if (input.files && input.files[0]) {
            filesArray.push(Number(fileIndex));
            filesArray.sort(function(a, b){return a - b})
            console.log(filesArray);
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#' + place + ' + img').remove();
                if (fileIndex == 1) {
                  $('#' + place).after('<img src="' + e.target.result + '" class="pic-view' + fileIndex + '" width="400px" height="270px" style="width: 400px; height: 270px; margin-top: 65px"/>');
                  $('#rotate' + fileIndex + "-left").removeAttr('disabled');
                  $('#rotate' + fileIndex + "-right").removeAttr('disabled');
                } else {
                  $('#' + place).after('<img src="' + e.target.result + '" class="pic-view' + fileIndex + '" width="193px" height="140px" style="width: 193px; height: 140px; margin-top: 27.5px;"/>');
                  $('#rotate' + fileIndex).show();
                }

            };
            reader.readAsDataURL(input.files[0]);
            $('.' + place).show();
        } else {
            if (fileIndex != 1) $('#' + place + ' + img').remove();
            filesArray = filesArray.filter(num => num != Number(fileIndex));
            filesArray.sort(function(a, b){return a - b});
            console.log(filesArray);
            $('.' + place).show();
            $('#rotate' + fileIndex).hide();
        }
    };

    $(document).ready(function () {
        $(document).on('mouseenter', '.img-preview', function () {
            $('.custom-file-input2').show();
        }).on('mouseleave', '.img-preview', function () {
            $('.custom-file-input2').hide();
        });
        $(document).on('mouseenter', '.custom-file-input2', function () {
            $('.custom-file-input2').show();
        }).on('mouseleave', '.custom-file-input2', function () {
                $('.custom-file-input2').hide();
        });
    });

    $(document).ready(function () {
        $(document).on('mouseenter', '.img-preview1', function () {
            $('.custom-in1').show();
        }).on('mouseleave', '.img-preview1', function () {
            $('.custom-in1').hide();
        });
        $(document).on('mouseenter', '.custom-in1', function () {
            $('.custom-in1').show();
        }).on('mouseleave', '.custom-in1', function () {
                $('.custom-in1').hide();
        });
    });

    $(document).ready(function () {
        $(document).on('mouseenter', '.img-preview2', function () {
            $('.custom-in2').show();
        }).on('mouseleave', '.img-preview2', function () {
            $('.custom-in2').hide();
        });
        $(document).on('mouseenter', '.custom-in2', function () {
            $('.custom-in2').show();
        }).on('mouseleave', '.custom-in2', function () {
                $('.custom-in2').hide();
        });
    });

    $(document).ready(function () {
        $(document).on('mouseenter', '.img-preview3', function () {
            $('.custom-in3').show();
        }).on('mouseleave', '.img-preview3', function () {
            $('.custom-in3').hide();
        });
        $(document).on('mouseenter', '.custom-in3', function () {
            $('.custom-in3').show();
        }).on('mouseleave', '.custom-in3', function () {
                $('.custom-in3').hide();
        });
    });

    $(document).ready(function () {
        $(document).on('mouseenter', '.img-preview4', function () {
            $('.custom-in4').show();
        }).on('mouseleave', '.img-preview4', function () {
            $('.custom-in4').hide();
        });
        $(document).on('mouseenter', '.custom-in4', function () {
            $('.custom-in4').show();
        }).on('mouseleave', '.custom-in4', function () {
                $('.custom-in4').hide();
        });
    });



</script>
    <script src="{{ asset('tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/tiny_script.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    
@endsection
